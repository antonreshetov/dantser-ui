import DInput from './package/input/Input.vue'
import DCheckbox from './package/checkbox/Checkbox.vue'
import DRadio from './package/radio/Radio.vue'
import DSelect from './package/select/Select.vue'
import DOption from './package/select/Option.vue'
import DDate from './package/date/Date.vue'
import DEditor from './package/editor/Editor.vue'
import DFancytree from './package/fancytree/Fancytree.vue'
import DModal from './package/modal/Modal.vue'
import DTabs from './package/tabs/Tabs.vue'
import DTabPane from './package/tabs/TabPane.vue'
import DTable from './package/table/Table.vue'
import DTableFilter from './package/table-filter/TableFilter.vue'
import DButton from './package/button/Button.vue'

const components = [
    DInput,
    DCheckbox,
    DRadio,
    DSelect,
    DOption,
    DButton,
    DTable,
    DTableFilter,
    DDate,
    DEditor,
    DFancytree,
    DTabs,
    DTabPane,
    DModal
];

const DantserUI = {
    install(Vue, options) {
        components.map(component => {
            Vue.component(component.name, component);
        });
    }
};
export default DantserUI;
