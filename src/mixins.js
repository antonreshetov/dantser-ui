export default {
    data() {
        return {
            // TODO: удaлить
            classDefault: 'form-control'
        }
    },
    methods: {
        /**
         * Проверяет есть ли у родительского компонента в data определенное свойство.
         * @param {string} property
         * @returns {boolean}
         */
        hasParentData(property) {
            return !!this.$parent.$data[property];
        },
        /**
         * @returns {string} - генерация произвольного ID
         */
        guid() {
            return Math.random().toString(36).substring(2,15);
        },
        /**
         * Удаление свойств объекта
         * @param {string} prop
         * @param {object} data
         */
        deleteProp(prop, data) {
            for (let i in data) {
                if (data[prop]) {
                    delete data[prop]
                }
                if (typeof data[i] === 'object') {
                    this.deleteProp(prop, data[i])
                }
            }
        }
    }
}