module.exports = function () {
    return {
        options: {
            filterFields: {
                "status": "true",
                "filter_fields": [
                    {
                        "id": "id",
                        "name": "ID",
                        "type": "input-text",
                        "sortable": true,
                        "current_value": ""
                    },
                    {
                        "id": "name",
                        "name": "Название",
                        "type": "input-text",
                        "sortable": true,
                        "current_value": ""
                    },
                    {
                        "id": "owner_id",
                        "name": "Владелец",
                        "type": "fancy-input",
                        "data": "http://127.0.0.1:3030/fancytreeData",
                        "current_value": {
                            "key": 3,
                            "title": "Программист(Иванов Иван Иванович)"
                        }
                    },
                    {
                        "id": "select_id",
                        "name": "Статус",
                        "type": "select",
                        "data": [
                            {
                                "key": "1",
                                "value": "Новый"
                            },
                            {
                                "key": "2",
                                "value": "Архив"
                            }
                        ],
                        "current_value": 1
                    },
                    {
                        "id": "created_at",
                        "name": "Дата создания",
                        "type": "date-range",
                        "sortable": true,
                        "current_value": "01.06.2017 00:00 - 30.06.2017 23:59"
                    }
                ],
                "buttons": [
                    {
                        "id": "default",
                        "name": "Кнопка 1",
                        "type": "system",
                        "sort": [
                            {
                                "filter_field_id": "name",
                                "direction": "desc"
                            },
                            {
                                "filter_field_id": "id",
                                "direction": "desc"
                            }
                        ],
                        "filter": [
                            {
                                "condition": "",
                                "rows": [
                                    {
                                        "condition": "",
                                        "operation": "gt",
                                        "field": {
                                            "filter_field_id": "id",
                                            "value": "10"
                                        }
                                    }
                                ],
                            },
                            {
                                "condition": "and",
                                "rows": [
                                    {
                                        "condition": "",
                                        "operation": "like",
                                        "field": {
                                            "filter_field_id": "name",
                                            "value": "fourth storage"
                                        }
                                    },
                                    {
                                        "condition": "or",
                                        "operation": "eq",
                                        "field": {
                                            "filter_field_id": "id",
                                            "value": "5"
                                        }
                                    },
                                    {
                                        "condition": "or",
                                        "operation": "eq",
                                        "field": {
                                            "filter_field_id": "name",
                                            "value": "Lorem"
                                        }
                                    }
                                ]
                            },
                            {
                                "condition": "or",
                                "rows": [
                                    {
                                        "condition": "or",
                                        "operation": "like",
                                        "field": {
                                            "filter_field_id": "owner_id",
                                            "value": "Программист(Иванов Иван Иванович)"
                                        }
                                    },
                                    {
                                        "condition": "and",
                                        "operation": "eq",
                                        "field": {
                                            "filter_field_id": "created_at",
                                            "value": "01.06.2017 00:00 - 30.06.2017 23:59"
                                        }
                                    }
                                ]
                            }
                        ],
                        "selected": false
                    },
                    {
                        "id": "default-2",
                        "name": "Кнопка 2",
                        "type": "system",
                        "sort": [
                            {
                                "filter_field_id": "name",
                                "direction": "desc"
                            },
                            {
                                "filter_field_id": "id",
                                "direction": "desc"
                            }
                        ],
                        "filter": [
                            {
                                "condition": "",
                                "rows": [
                                    {
                                        "condition": "",
                                        "operation": "gt",
                                        "field": {
                                            "filter_field_id": "id",
                                            "value": "10"
                                        }
                                    }
                                ],
                            },
                            {
                                "condition": "and",
                                "rows": [
                                    {
                                        "condition": "",
                                        "operation": "like",
                                        "field": {
                                            "filter_field_id": "name",
                                            "value": "fourth storage"
                                        }
                                    }
                                ]
                            },
                            {
                                "condition": "or",
                                "rows": [
                                    {
                                        "condition": "or",
                                        "operation": "like",
                                        "field": {
                                            "filter_field_id": "owner_id",
                                            "value": "Программист(Иванов Иван Иванович)"
                                        }
                                    },
                                    {
                                        "condition": "and",
                                        "operation": "eq",
                                        "field": {
                                            "filter_field_id": "created_at",
                                            "value": "01.06.2017 00:00 - 30.06.2017 23:59"
                                        }
                                    }
                                ]
                            }
                        ],
                        "selected": true
                    },
                    {
                        "id": "default-3",
                        "name": "Кнопка 3",
                        "type": "system",
                        "sort": [
                            {
                                "filter_field_id": "name",
                                "direction": "desc"
                            },
                            {
                                "filter_field_id": "id",
                                "direction": "desc"
                            }
                        ],
                        "filter": [
                            {
                                "condition": "",
                                "rows": [
                                    {
                                        "condition": "",
                                        "operation": "gt",
                                        "field": {
                                            "filter_field_id": "id",
                                            "value": "10"
                                        }
                                    }
                                ],
                            },
                            {
                                "condition": "and",
                                "rows": [
                                    {
                                        "condition": "",
                                        "operation": "like",
                                        "field": {
                                            "filter_field_id": "name",
                                            "value": "fourth storage"
                                        }
                                    },
                                    {
                                        "condition": "or",
                                        "operation": "eq",
                                        "field": {
                                            "filter_field_id": "id",
                                            "value": "5"
                                        }
                                    },
                                    {
                                        "condition": "or",
                                        "operation": "eq",
                                        "field": {
                                            "filter_field_id": "name",
                                            "value": "Lorem"
                                        }
                                    }
                                ]
                            },
                            {
                                "condition": "or",
                                "rows": [
                                    {
                                        "condition": "or",
                                        "operation": "like",
                                        "field": {
                                            "filter_field_id": "owner_id",
                                            "value": "Программист(Иванов Иван Иванович)"
                                        }
                                    },
                                    {
                                        "condition": "and",
                                        "operation": "eq",
                                        "field": {
                                            "filter_field_id": "created_at",
                                            "value": "01.06.2017 00:00 - 30.06.2017 23:59"
                                        }
                                    }
                                ]
                            }
                        ],
                        "selected": false
                    }
                ]
            }
        },
        fancytreeData: [
                { title: "Node 1", key: "1", color: "#ccc" },
                {
                    title: "Folder 2",
                    key: "2",
                    folder: true,
                    children: [
                        { title: "Программист(Иванов Иван Иванович)", key: "3" },
                        { title: "Node 2.2", key: "4" },
                        {
                            title: "Node 2.3",
                            key: "5",
                            folder: true,
                            children: [
                                { title: "Node 2.3.1", key: "6" },
                                { title: "Node 2.3.2", key: "7" },
                            ]
                        },
                    ]
                }
            ]
    }
}
