import Vue from 'vue'
import Router from './router'
import DantserUI from '../src/index'

import Index from './Index.vue'

window.axios = require('axios');

Vue.use(Router);
Vue.use(DantserUI);

if (document.getElementById('components') !== null) {
    new Vue({
        el: '#components',
        router: Router,
        render: h => h(Index)
    });
}
