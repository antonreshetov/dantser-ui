import Vue from 'vue'
import VueRouter from 'vue-router'

import Index from './components/Index.vue'
import Input from './components/Input.vue'
import Checkbox from './components/Checkbox.vue'
import Radio from './components/Radio.vue'
import Select from './components/Select.vue'
import Date from './components/Date.vue'
import Editor from './components/Editor.vue'
import Fancytree from './components/Fancytree.vue'
import Modal from './components/Modal.vue'
import Tabs from './components/Tabs.vue'
import Table from './components/Table.vue'
import TableFilter from './components/TableFilter.vue'
import Buttons from './components/Buttons.vue'

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        {
            path: '/',
            component: Index
        },
        {
            path: '/input',
            component: Input
        },
        {
            path: '/checkbox',
            component: Checkbox
        },
        {
            path: '/radio',
            component: Radio
        },
        {
            path: '/select',
            component: Select
        },
        {
            path: '/buttons',
            component: Buttons
        },
        {
            path: '/table',
            component: Table
        },
        {
            path: '/table-filter',
            component: TableFilter
        },
        {
            path: '/date',
            component: Date
        },
        {
            path: '/editor',
            component: Editor
        },
        {
            path: '/fancytree',
            component: Fancytree
        },
        {
            path: '/modal',
            component: Modal
        },
        {
            path: '/tabs',
            component: Tabs
        },

    ]
});

export default router